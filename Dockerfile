FROM python:2.7.18-slim-buster

RUN apt-get update \
    && apt-get -y upgrade \
    && apt-get install -y build-essential wget zlib1g-dev libncurses5-dev \
    && apt-get clean \
    && apt-get purge \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
    && pip install scipy
